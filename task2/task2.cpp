﻿#include <iostream>
#include <chrono>
#include <cmath>

int main() {
    // Початок вимірювання часу
    auto start = std::chrono::high_resolution_clock::now();

    // Виконуємо обчислення квадратних коренів для чисел від 0 до 999999
    for (int i = 0; i < 1000000; i++) {
        double x = sqrt(i);
    }

    // Завершення вимірювання часу
    auto end = std::chrono::high_resolution_clock::now();

    // Визначаємо тривалість виконання коду
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    // Виводимо результат
    std::cout << "Code execution took: " << duration.count() << " milliseconds" << std::endl;

    return 0;
}