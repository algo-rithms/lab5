﻿#include <stdio.h>

void insertionSort(int arr[], int n) {
    for (int i = 1; i < n; i++) {
        int key = arr[i];
        int j = i - 1;

        // Порівнюємо елемент key з відсортованими елементами і вставляємо його на потрібне місце
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];  // Здвигаємо елементи вправо
            j = j - 1;
        }
        arr[j + 1] = key;  // Вставляємо key на правильне місце у відсортованій частині масиву
    }
}

// Вивід масиву
void printArray(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() {
    int arr[] = { 12, 11, 13, 5, 6 };
    int n = sizeof(arr) / sizeof(arr[0]);

    printf("Before sorting: ");
    printArray(arr, n);

    insertionSort(arr, n);

    printf("After sorting: ");
    printArray(arr, n);

    return 0;
}
