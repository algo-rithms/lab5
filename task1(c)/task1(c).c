﻿#include <stdio.h>
#include <stdlib.h>

// Оголошення структури для вузла двонапрямленого списку
typedef struct Node {
    int data;           // Дані, що містяться в вузлі
    struct Node* next;  // Вказівник на наступний вузол
    struct Node* prev;  // Вказівник на попередній вузол
} Node;

// Функція для створення нового вузла з заданим значенням даних
Node* createNode(int data) {
    // Виділення пам'яті для нового вузла
    Node* newNode = (Node*)malloc(sizeof(Node));
    // Ініціалізація даних вузла та вказівників на NULL
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;  // Повертаємо вказівник на новий вузол
}

// Функція для додавання нового вузла з заданим значенням даних в кінець списку
void appendNode(Node** head, int data) {
    // Створення нового вузла з заданим значенням даних
    Node* newNode = createNode(data);
    if (*head == NULL) {  // Якщо список порожній
        *head = newNode;  // Робимо новий вузол головою списку
        return;
    }
    // Якщо список не порожній, шукаємо останній вузол та додаємо новий вузол після нього
    Node* temp = *head;
    while (temp->next != NULL) {  // Переміщаємось до кінця списку
        temp = temp->next;
    }
    temp->next = newNode;  // Додаємо новий вузол після останнього вузла
    newNode->prev = temp;  // Встановлюємо попередній вузол для нового вузла
}

void insertionSort(Node** head) {
    if (*head == NULL || (*head)->next == NULL) {
        return; // Перевірка на порожній або одноелементний список
    }
    Node* sorted = NULL; // Ініціалізація відсортованого списку
    Node* current = *head; // Початок несортованого списку

    while (current != NULL) {
        Node* next = current->next; // Зберігаємо наступний вузол перед зміною посилань
        current->prev = current->next = NULL; // Розділяємо поточний вузол від списку

        // Вставляємо поточний вузол у відсортований список
        if (sorted == NULL || sorted->data >= current->data) {
            current->next = sorted;
            if (sorted != NULL) {
                sorted->prev = current;
            }
            sorted = current;
        }
        else {
            Node* temp = sorted;
            while (temp->next != NULL && temp->next->data < current->data) {
                temp = temp->next;
            }
            current->next = temp->next;
            if (temp->next != NULL) {
                temp->next->prev = current;
            }
            temp->next = current;
            current->prev = temp;
        }
        current = next; // Переміщаємося до наступного елемента в несортованому списку
    }
    *head = sorted; // Поновлюємо голову списку на відсортований список
}

void printList(Node* head) {
    Node* temp = head;
    while (temp != NULL) {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

int main() {
    Node* head = NULL;
    appendNode(&head, 3);
    appendNode(&head, 1);
    appendNode(&head, 4);
    appendNode(&head, 2);

    printf("Before sorting: ");
    printList(head);

    insertionSort(&head);

    printf("After sorting: ");
    printList(head);

    return 0;
}
