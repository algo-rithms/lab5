﻿#include <stdio.h>
#include <stdlib.h>

// Оголошення структури для вузла двонапрямленого списку
typedef struct Node {
    int data;           // Дані, що містяться в вузлі
    struct Node* next;  // Вказівник на наступний вузол
    struct Node* prev;  // Вказівник на попередній вузол
} Node;

// Функція для створення нового вузла з заданим значенням даних
Node* createNode(int data) {
    // Виділення пам'яті для нового вузла
    Node* newNode = (Node*)malloc(sizeof(Node));
    // Ініціалізація даних вузла та вказівників на NULL
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;  // Повертаємо вказівник на новий вузол
}

// Функція для додавання нового вузла з заданим значенням даних в кінець списку
void appendNode(Node** head, int data) {
    // Створення нового вузла з заданим значенням даних
    Node* newNode = createNode(data);
    if (*head == NULL) {  // Якщо список порожній
        *head = newNode;  // Робимо новий вузол головою списку
        return;
    }
    // Якщо список не порожній, шукаємо останній вузол та додаємо новий вузол після нього
    Node* temp = *head;
    while (temp->next != NULL) {  // Переміщаємось до кінця списку
        temp = temp->next;
    }
    temp->next = newNode;  // Додаємо новий вузол після останнього вузла
    newNode->prev = temp;  // Встановлюємо попередній вузол для нового вузла
}

void selectionSort(Node* head) {
    Node* temp, * min;
    int tempData;

    // Ітеруємося по списку
    for (temp = head; temp != NULL; temp = temp->next) {
        min = temp;
        // Знаходимо мінімальний елемент в залишковій частині списку
        for (Node* r = temp->next; r != NULL; r = r->next) {
            if (r->data < min->data) {
                min = r;
            }
        }
        // Обмін значень між поточним елементом та мінімальним елементом
        if (min != temp) {
            tempData = temp->data;
            temp->data = min->data;
            min->data = tempData;
        }
    }
}

void printList(Node* head) {
    Node* temp = head;
    while (temp != NULL) {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

int main() {
    Node* head = NULL;
    appendNode(&head, 3);
    appendNode(&head, 1);
    appendNode(&head, 4);
    appendNode(&head, 2);

    printf("Before sorting: ");
    printList(head);

    selectionSort(head);

    printf("After sorting: ");
    printList(head);

    return 0;
}
